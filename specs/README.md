# Military Specifications for Waterproof Rubber Shell "Packard" Connectors

Title                                                                              | Specification                              | Date
-----------------------------------------------------------------------------------|--------------------------------------------|-----------
Contact, Electrical, No. 12, 14, and 16 AWG                                        | [A-A-52536](A-A-52536.pdf)                 | 1995-11-20
Connector, Plug, Electrical, Pin Contact, No. 12, 14, and 16 AWG, Waterproof       | [MS27142E](MS27142E.pdf)                   | 2018-03-01
Connector, Plug, Electrical, Socket Contact, No. 12 AWG, Waterproof                | [MS27143D](MS27143D.pdf)                   | 2018-03-01
Connector, Plug, Electrical, Socket Contact, No. 14 and 16 AWG, Waterproof         | [MS27144D](MS27144D.pdf)                   | 2016-07-12
Connector, Plug, Electrical, No. 14 AWG, Double Contact, Waterproof                | [MS27145D](MS27145D.pdf)                   | 2018-03-20
Connector, Receptacle, Electrical, Double Contact, Waterproof                      | [MS27146D](MS27146D.pdf)                   | 2018-03-20
Adapter, Connector, 3-Way, No. 12, 14 and 16 AWG, Waterproof                       | [MS27147D](MS27147D.pdf)                   | 2003-06-17
Contact, Electrical, No. 12, 14 and 16 AWG                                         | [MS27148C](MS27148C.pdf)                   | 1969-04-28
Contact, Electrical, No. 12, 14 and 16 AWG: Note of Cancellation                   | [MS27148C NOTICE 1](MS27148C_NOTICE-1.pdf) | 1995-11-20
Connector, Receptacle, Electrical, Pin Contact, No. 12, 14, and 16 AWG, Waterproof | [MS33800F](MS33800F.pdf)                   | 2018-03-28
