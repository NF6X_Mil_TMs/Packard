# Waterproof Rubber Shell "Packard" Connectors

This is my personal cache of documents related to waterproof rubber shell "Packard" connectors which are commonly used in US military tactical vehicle electrical systems.

The documents are sorted into these subdirectories:

Directory                | Description
-------------------------|---------------------------------------
[specs](specs/README.md) | US military specifications
[misc](misc/README.md)   | Miscellaneous documents
