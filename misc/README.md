# Miscellaneous Documents

These are miscellaneous documents related to 

Description                                      | File 
-------------------------------------------------|-----------------------------------------
Table of Daniels crimp dies                      | [DMC_Crimp_Dies.pdf](DMC_Crimp_Dies.pdf)
Daniels HX4 open frame hand crimp tool           | [HX4_ds.pdf](HX4_ds.pdf)
Daniels crimp die for MS27148/A-A-52536 contacts | [Y627_env.pdf](Y627_env.pdf)
